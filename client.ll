; ModuleID = 'client.bc'
source_filename = "client3.c"
target datalayout = "e-m:o-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-apple-macosx10.15.0"

%struct.__sFILE = type { i8*, i32, i32, i16, i16, %struct.__sbuf, i32, i8*, i32 (i8*)*, i32 (i8*, i8*, i32)*, i64 (i8*, i64, i32)*, i32 (i8*, i8*, i32)*, %struct.__sbuf, %struct.__sFILEX*, i32, [3 x i8], [1 x i8], %struct.__sbuf, i32, i64 }
%struct.__sFILEX = type opaque
%struct.__sbuf = type { i8*, i32 }
%struct.ossl_init_settings_st = type opaque
%struct.ssl_ctx_st = type opaque
%struct.bio_st = type opaque
%struct.ssl_st = type opaque
%struct.ssl_method_st = type opaque

@__stderrp = external local_unnamed_addr global %struct.__sFILE*, align 8
@.str = private unnamed_addr constant [25 x i8] c"TLSv1_2_client_method...\00", align 1
@.str.1 = private unnamed_addr constant [15 x i8] c"SSL_CTX_new...\00", align 1
@.str.2 = private unnamed_addr constant [23 x i8] c"BIO_new_ssl_connect...\00", align 1
@.str.3 = private unnamed_addr constant [6 x i8] c"%s:%s\00", align 1
@.str.4 = private unnamed_addr constant [6 x i8] c"https\00", align 1
@.str.5 = private unnamed_addr constant [18 x i8] c"BIO_do_connect...\00", align 1
@.str.6 = private unnamed_addr constant [35 x i8] c"/etc/ssl/certs/ca-certificates.crt\00", align 1
@.str.7 = private unnamed_addr constant [16 x i8] c"/etc/ssl/certs/\00", align 1
@.str.8 = private unnamed_addr constant [33 x i8] c"SSL_CTX_load_verify_locations...\00", align 1
@.str.9 = private unnamed_addr constant [61 x i8] c"##### Certificate verification error (%i) but continuing...\0A\00", align 1
@.str.10 = private unnamed_addr constant [48 x i8] c"GET / HTTP/1.1\0D\0AHost: %s\0D\0AConnection: Close\0D\0A\0D\0A\00", align 1
@.str.11 = private unnamed_addr constant [19 x i8] c"www.google.com:443\00", align 1
@.str.12 = private unnamed_addr constant [37 x i8] c"Trying an HTTPS connection to %s...\0A\00", align 1

; Function Attrs: noreturn nounwind ssp uwtable
define void @report_and_exit(i8* nocapture readonly %0) local_unnamed_addr #0 {
  tail call void @perror(i8* %0) #9
  %2 = load %struct.__sFILE*, %struct.__sFILE** @__stderrp, align 8, !tbaa !3
  tail call void @ERR_print_errors_fp(%struct.__sFILE* %2) #10
  tail call void @exit(i32 -1) #11
  unreachable
}

; Function Attrs: cold nofree nounwind
declare void @perror(i8* nocapture readonly) local_unnamed_addr #1

declare void @ERR_print_errors_fp(%struct.__sFILE*) local_unnamed_addr #2

; Function Attrs: noreturn
declare void @exit(i32) local_unnamed_addr #3

; Function Attrs: nounwind ssp uwtable
define void @init_ssl() local_unnamed_addr #4 {
  %1 = tail call i32 @OPENSSL_init_ssl(i64 2097154, %struct.ossl_init_settings_st* null) #10
  %2 = tail call i32 @OPENSSL_init_ssl(i64 0, %struct.ossl_init_settings_st* null) #10
  ret void
}

declare i32 @OPENSSL_init_ssl(i64, %struct.ossl_init_settings_st*) local_unnamed_addr #2

; Function Attrs: nounwind ssp uwtable
define void @cleanup(%struct.ssl_ctx_st* %0, %struct.bio_st* %1) local_unnamed_addr #4 {
  tail call void @SSL_CTX_free(%struct.ssl_ctx_st* %0) #10
  tail call void @BIO_free_all(%struct.bio_st* %1) #10
  ret void
}

declare void @SSL_CTX_free(%struct.ssl_ctx_st*) local_unnamed_addr #2

declare void @BIO_free_all(%struct.bio_st*) local_unnamed_addr #2

; Function Attrs: nounwind ssp uwtable
define void @secure_connect(i8* %0) local_unnamed_addr #4 {
  %2 = alloca [1024 x i8], align 16
  %3 = alloca [1024 x i8], align 16
  %4 = alloca [1024 x i8], align 16
  %5 = alloca %struct.ssl_st*, align 8
  %6 = getelementptr inbounds [1024 x i8], [1024 x i8]* %2, i64 0, i64 0
  call void @llvm.lifetime.start.p0i8(i64 1024, i8* nonnull %6) #10
  %7 = getelementptr inbounds [1024 x i8], [1024 x i8]* %3, i64 0, i64 0
  call void @llvm.lifetime.start.p0i8(i64 1024, i8* nonnull %7) #10
  %8 = getelementptr inbounds [1024 x i8], [1024 x i8]* %4, i64 0, i64 0
  call void @llvm.lifetime.start.p0i8(i64 1024, i8* nonnull %8) #10
  %9 = tail call %struct.ssl_method_st* @TLSv1_2_client_method() #10
  %10 = icmp eq %struct.ssl_method_st* %9, null
  br i1 %10, label %11, label %12

11:                                               ; preds = %1
  tail call void @report_and_exit(i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str, i64 0, i64 0))
  unreachable

12:                                               ; preds = %1
  %13 = tail call %struct.ssl_ctx_st* @SSL_CTX_new(%struct.ssl_method_st* nonnull %9) #10
  %14 = icmp eq %struct.ssl_ctx_st* %13, null
  br i1 %14, label %15, label %16

15:                                               ; preds = %12
  tail call void @report_and_exit(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.1, i64 0, i64 0))
  unreachable

16:                                               ; preds = %12
  %17 = tail call %struct.bio_st* @BIO_new_ssl_connect(%struct.ssl_ctx_st* nonnull %13) #10
  %18 = icmp eq %struct.bio_st* %17, null
  br i1 %18, label %19, label %20

19:                                               ; preds = %16
  tail call void @report_and_exit(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.2, i64 0, i64 0))
  unreachable

20:                                               ; preds = %16
  %21 = bitcast %struct.ssl_st** %5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* nonnull %21) #10
  store %struct.ssl_st* null, %struct.ssl_st** %5, align 8, !tbaa !3
  %22 = call i32 (i8*, i32, i64, i8*, ...) @__sprintf_chk(i8* nonnull %6, i32 0, i64 1024, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.3, i64 0, i64 0), i8* %0, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.4, i64 0, i64 0)) #10
  %23 = call i64 @BIO_ctrl(%struct.bio_st* nonnull %17, i32 110, i64 0, i8* nonnull %21) #10
  %24 = load %struct.ssl_st*, %struct.ssl_st** %5, align 8, !tbaa !3
  %25 = call i64 @SSL_ctrl(%struct.ssl_st* %24, i32 33, i64 4, i8* null) #10
  %26 = call i64 @BIO_ctrl(%struct.bio_st* nonnull %17, i32 100, i64 0, i8* nonnull %6) #10
  %27 = call i64 @BIO_ctrl(%struct.bio_st* nonnull %17, i32 101, i64 0, i8* null) #10
  %28 = icmp slt i64 %27, 1
  br i1 %28, label %29, label %30

29:                                               ; preds = %20
  call void @cleanup(%struct.ssl_ctx_st* nonnull %13, %struct.bio_st* nonnull %17)
  call void @report_and_exit(i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.5, i64 0, i64 0))
  unreachable

30:                                               ; preds = %20
  %31 = call i32 @SSL_CTX_load_verify_locations(%struct.ssl_ctx_st* nonnull %13, i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str.6, i64 0, i64 0), i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.7, i64 0, i64 0)) #10
  %32 = icmp eq i32 %31, 0
  br i1 %32, label %33, label %34

33:                                               ; preds = %30
  call void @report_and_exit(i8* getelementptr inbounds ([33 x i8], [33 x i8]* @.str.8, i64 0, i64 0))
  unreachable

34:                                               ; preds = %30
  %35 = load %struct.ssl_st*, %struct.ssl_st** %5, align 8, !tbaa !3
  %36 = call i64 @SSL_get_verify_result(%struct.ssl_st* %35) #10
  %37 = icmp eq i64 %36, 0
  br i1 %37, label %42, label %38

38:                                               ; preds = %34
  %39 = load %struct.__sFILE*, %struct.__sFILE** @__stderrp, align 8, !tbaa !3
  %40 = trunc i64 %36 to i32
  %41 = call i32 (%struct.__sFILE*, i8*, ...) @fprintf(%struct.__sFILE* %39, i8* getelementptr inbounds ([61 x i8], [61 x i8]* @.str.9, i64 0, i64 0), i32 %40)
  br label %42

42:                                               ; preds = %34, %38
  %43 = call i32 (i8*, i32, i64, i8*, ...) @__sprintf_chk(i8* nonnull %7, i32 0, i64 1024, i8* getelementptr inbounds ([48 x i8], [48 x i8]* @.str.10, i64 0, i64 0), i8* %0) #10
  %44 = call i32 @BIO_puts(%struct.bio_st* nonnull %17, i8* nonnull %7) #10
  call void @llvm.memset.p0i8.i64(i8* nonnull align 16 dereferenceable(1024) %8, i8 0, i64 1024, i1 false)
  %45 = call i32 @BIO_read(%struct.bio_st* nonnull %17, i8* nonnull %8, i32 1024) #10
  %46 = icmp slt i32 %45, 1
  br i1 %46, label %51, label %47

47:                                               ; preds = %42, %47
  %48 = call i32 @puts(i8* nonnull %8)
  call void @llvm.memset.p0i8.i64(i8* nonnull align 16 dereferenceable(1024) %8, i8 0, i64 1024, i1 false)
  %49 = call i32 @BIO_read(%struct.bio_st* nonnull %17, i8* nonnull %8, i32 1024) #10
  %50 = icmp slt i32 %49, 1
  br i1 %50, label %51, label %47

51:                                               ; preds = %47, %42
  call void @SSL_CTX_free(%struct.ssl_ctx_st* nonnull %13) #10
  call void @BIO_free_all(%struct.bio_st* nonnull %17) #10
  call void @llvm.lifetime.end.p0i8(i64 8, i8* nonnull %21) #10
  call void @llvm.lifetime.end.p0i8(i64 1024, i8* nonnull %8) #10
  call void @llvm.lifetime.end.p0i8(i64 1024, i8* nonnull %7) #10
  call void @llvm.lifetime.end.p0i8(i64 1024, i8* nonnull %6) #10
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #5

declare %struct.ssl_method_st* @TLSv1_2_client_method() local_unnamed_addr #2

declare %struct.ssl_ctx_st* @SSL_CTX_new(%struct.ssl_method_st*) local_unnamed_addr #2

declare %struct.bio_st* @BIO_new_ssl_connect(%struct.ssl_ctx_st*) local_unnamed_addr #2

; Function Attrs: nofree
declare i32 @__sprintf_chk(i8*, i32, i64, i8*, ...) local_unnamed_addr #6

declare i64 @BIO_ctrl(%struct.bio_st*, i32, i64, i8*) local_unnamed_addr #2

declare i64 @SSL_ctrl(%struct.ssl_st*, i32, i64, i8*) local_unnamed_addr #2

declare i32 @SSL_CTX_load_verify_locations(%struct.ssl_ctx_st*, i8*, i8*) local_unnamed_addr #2

declare i64 @SSL_get_verify_result(%struct.ssl_st*) local_unnamed_addr #2

; Function Attrs: nofree nounwind
declare i32 @fprintf(%struct.__sFILE* nocapture, i8* nocapture readonly, ...) local_unnamed_addr #7

declare i32 @BIO_puts(%struct.bio_st*, i8*) local_unnamed_addr #2

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i64(i8* nocapture writeonly, i8, i64, i1 immarg) #8

declare i32 @BIO_read(%struct.bio_st*, i8*, i32) local_unnamed_addr #2

; Function Attrs: nofree nounwind
declare i32 @puts(i8* nocapture readonly) local_unnamed_addr #7

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #5

; Function Attrs: nounwind ssp uwtable
define i32 @main() local_unnamed_addr #4 {
  %1 = tail call i32 @OPENSSL_init_ssl(i64 2097154, %struct.ossl_init_settings_st* null) #10
  %2 = tail call i32 @OPENSSL_init_ssl(i64 0, %struct.ossl_init_settings_st* null) #10
  %3 = load %struct.__sFILE*, %struct.__sFILE** @__stderrp, align 8, !tbaa !3
  %4 = tail call i32 (%struct.__sFILE*, i8*, ...) @fprintf(%struct.__sFILE* %3, i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.12, i64 0, i64 0), i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.11, i64 0, i64 0))
  tail call void @secure_connect(i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.11, i64 0, i64 0))
  ret i32 0
}

attributes #0 = { noreturn nounwind ssp uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="penryn" "target-features"="+cx16,+cx8,+fxsr,+mmx,+sahf,+sse,+sse2,+sse3,+sse4.1,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { cold nofree nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="penryn" "target-features"="+cx16,+cx8,+fxsr,+mmx,+sahf,+sse,+sse2,+sse3,+sse4.1,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="penryn" "target-features"="+cx16,+cx8,+fxsr,+mmx,+sahf,+sse,+sse2,+sse3,+sse4.1,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="penryn" "target-features"="+cx16,+cx8,+fxsr,+mmx,+sahf,+sse,+sse2,+sse3,+sse4.1,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind ssp uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="penryn" "target-features"="+cx16,+cx8,+fxsr,+mmx,+sahf,+sse,+sse2,+sse3,+sse4.1,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { argmemonly nounwind willreturn }
attributes #6 = { nofree "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="penryn" "target-features"="+cx16,+cx8,+fxsr,+mmx,+sahf,+sse,+sse2,+sse3,+sse4.1,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nofree nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="penryn" "target-features"="+cx16,+cx8,+fxsr,+mmx,+sahf,+sse,+sse2,+sse3,+sse4.1,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { argmemonly nounwind willreturn writeonly }
attributes #9 = { cold }
attributes #10 = { nounwind }
attributes #11 = { noreturn nounwind }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.1.0"}
!3 = !{!4, !4, i64 0}
!4 = !{!"any pointer", !5, i64 0}
!5 = !{!"omnipotent char", !6, i64 0}
!6 = !{!"Simple C/C++ TBAA"}
