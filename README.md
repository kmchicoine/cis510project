Note: I didn't include the code for all of the passes because they are intertwined with the source code for LLVM.  If I were to distribute this code to others, I would create statically linked libraries which are more modular and more easily sharable.  

Client/server modeled after code found here:
https://opensource.com/article/19/6/cryptography-basics-openssl-part-1
